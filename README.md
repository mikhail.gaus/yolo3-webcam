# YOLO3-webcam

![](https://pjreddie.com/media/image/Screen_Shot_2018-03-24_at_10.48.42_PM.png)

This is the absolute minimum you need to test YOLO3 with your webcam.

Prerequisites are `python3` and `pip` installed.

1. Clone this repository.
`git clone https://gitlab.com/mikhail.gaus/yolo3-webcam.git`
2. Install opencv:
`pip install opencv-python`

3. Install Pillow:
`pip install Pillow`

4. run it and enjoy the fastest object detection algorithm:
`python webcam.py`

It's actually not so fast, because I couldn't configure it to run with GPU. 

Tested on Ubuntu 20 and Windows 10 Home.


### Big thanks to:
https://pjreddie.com/darknet/yolo/

https://pjreddie.com/media/files/papers/YOLOv3.pdf

https://medium.com/analytics-vidhya/real-time-object-detection-using-yolov3-with-opencv-and-python-64c985e14786